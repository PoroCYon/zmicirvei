# zmicirvei

Create a git commit as soon as a file is written to.

## Usage

```
zmicirvei - commit as soon as a file is written to.

Usage: zmicirvei [<basedir>]
    basedir:    The base directory to watch. If unspecified, the current
                directory will be used.

    NOTE: Files not already in the git repository will *not* be added
    automatically.

Example:
    zmicirvei src/
```

## Dependencies

* `bash`
* `inotifywait`
* `git`

## License

This script is too small to be considered a 'creative work'. Do whatever you
want with it.

