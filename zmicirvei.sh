#!/usr/bin/env bash

case "$@" in
    "--help"|"-h")
        BASE="$(basename "$0")"
        cat <<EOF
$BASE - commit as soon as a file is written to.

Usage: $BASE [<basedir>]
    basedir:    The base directory to watch. If unspecified, the current
                directory will be used.

    NOTE: Files not already in the git repository will *not* be added
    automatically.

Example:
    $BASE src/
EOF
        exit
        ;;
    *)
        ;;
esac

BASEDIR="$1"

if [ -z "$BASEDIR" ]; then
    BASEDIR="."
fi

inotifywait -q -m -e modify --format '%f' -r "$BASEDIR/" | while read -r FILE; do
    git ls-files --error-unmatch "$FILE" >/dev/null 2>&1 && \
        echo -n "$FILE " && date && \
        git add "$FILE" >/dev/null 2>&1 && \
        git commit -m "Modified $FILE" >/dev/null 2>&1
done

